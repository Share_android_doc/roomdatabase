package com.somongkol.roomdemo.AppDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.somongkol.roomdemo.dao.UserDao;
import com.somongkol.roomdemo.entity.User;


@Database(entities = {User.class},version = 1)
public abstract class AppDatabase extends RoomDatabase {

    static String mydb="user_db";
    public abstract UserDao userDao();
    public static AppDatabase INSTANCE;

   public static  AppDatabase getINSTANCE(Context context){
       if (INSTANCE ==null){
           INSTANCE = Room.databaseBuilder(context,AppDatabase.class,mydb)
                   .allowMainThreadQueries()
                   .build();
       }
       return INSTANCE;
    }


}
