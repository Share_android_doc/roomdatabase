package com.somongkol.roomdemo.dao;



import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.somongkol.roomdemo.entity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Insert
    void insertAll(User... users);
    @Query("SELECT * FROM user")
    List<User> getUser();


    @Delete
    void delete(User user);

}
