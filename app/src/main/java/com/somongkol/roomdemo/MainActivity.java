package com.somongkol.roomdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


import com.somongkol.roomdemo.AppDatabase.AppDatabase;
import com.somongkol.roomdemo.dao.UserDao;
import com.somongkol.roomdemo.entity.User;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnSave;
    ListView listView;
    EditText editText;
    List<String> userlist;
    List<User> users;
    User user;
    AppDatabase appDatabase;
    UserDao dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSave = findViewById(R.id.btnSave);
        listView = findViewById(R.id.lvResult);
        editText = findViewById(R.id.edUser);

        user = new User();
        users = new ArrayList<>();
        user.setuName("Dalay");
        appDatabase = AppDatabase.getINSTANCE(this);
        dao = appDatabase.userDao();
        dao.insertUser(user);

        userlist = new ArrayList<>();
        for (User user : dao.getUser()){
            userlist.add(user.getuName().toString());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, userlist);
        listView.setAdapter(arrayAdapter);



    }
}
