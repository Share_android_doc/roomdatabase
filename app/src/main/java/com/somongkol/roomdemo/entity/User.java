package com.somongkol.roomdemo.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User  {
    @PrimaryKey(autoGenerate = true)
    public int uid;
   // @ColumnInfo(name = "username")
    public String uName;

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", uName='" + uName + '\'' +
                '}';
    }
}
